<?php

namespace wechat\src;
class Config
{
    private $appId = 'wx51e2a71175880078';
    private $secret = '56a7ff838bbf651257b38cf769fb5685';
    private $token = 'NpVWnxgwNjOat3fh0KG0QwED';
    private $redirect_uri = 'https://app1.taxingtianji.com/outdoor_smart_bike/web/html/login.html';

    /**
     * @param string $appId
     */
    public function setAppId(string $appId): void
    {
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $secret
     */
    public function setSecret(string $secret): void
    {
        $this->secret = $secret;
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getRedirectUri(): string
    {
        return $this->redirect_uri;
    }
}
