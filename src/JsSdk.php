<?php

namespace wechat\src;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class JsSdk
{
    public static function getSignPackage(string $url): array
    {
        $jsapiTicket = self::getJsApiTicket();
        // 注意 URL 一定要动态获取，不能 hardcode.
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || (int)$_SERVER['SERVER_PORT'] === 443) ? "https://" : "http://";
//        $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $timestamp = time();
        $nonceStr = self::createNonceStr(17);
        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
        $signature = sha1($string);
        return [
            "appId" => $_ENV['WECHAT_APPID'],
            "nonceStr" => $nonceStr,
            "timestamp" => $timestamp,
            "url" => $url,
            "signature" => $signature,
            "rawString" => $string
        ];
    }

    public static function createNonceStr(int $length = 16): string
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[random_int(0, strlen($chars) - 1)];
        }
        return $str;
    }

    /**
     * 获取ticket
     * @return string
     */
    public static function getJsApiTicket():string
    {
        $key = $_ENV['WECHAT_APPID'] . '_ticket';
        $redis = Redis::getInstance();
        $ticket = $redis->get($key);
        if (!$ticket) {
            $access_token = self::getAccessToken();
            // 如果是企业号用以下 URL 获取 ticket
            // $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
            $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=%s";
            $url = sprintf($url, $access_token);
            $content = self::httpGet($url);
            $result = json_decode($content, true);
            if ($result['errcode'] !== 0 && $result['errmsg']) {
                throw new WechatRuntimeException('获取ticket失败，错误码 ' . $result['errcode'] . ' 错误信息：' . $result['errmsg']);
            }
            $ticket = $result['ticket'];
            $redis->set($key, $ticket, 7100);
        }
        return $ticket;
    }

    /**
     * @return string
     */
    public static function getAccessToken(): string
    {
        $key = $_ENV['WECHAT_APPID'] . 'JSSdk_ACCESS_TOKEN';
        $redis = Redis::getInstance();
        $access_token = $redis->get($key);
        if (!$access_token) {
            // 如果是企业号用以下URL获取access_token
            // $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$this->appId&corpsecret=$this->appSecret";
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
            $url = sprintf($url, $_ENV['WECHAT_APPID'], $_ENV['WECHAT_APPSECRET']);
            $content = self::httpGet($url);
            Log::debug('获取JsSdk access token：' . $content);
            $result = json_decode($content, true);
            if (isset($result['errcode'])) {
                throw new WechatRuntimeException('获取AccessToken失败');
            }
            $redis->set($key, $result['access_token'], 7100);
            $access_token = $result['access_token'];
        }
        return $access_token;
    }

    private static function httpGet($url): string
    {
        $client = new Client();
        $response = $client->get($url, ['http_errors' => false]);
        return $response->getBody()->getContents();
    }
}

