<?php

namespace wechat\src\reply;

interface MessageInterface
{
    public function __construct($toUserName, $fromUserName, $message);

    public function create();

    public function reply();
}
