<?php


namespace wechat\src\reply;


use Illuminate\Support\Facades\Log;

abstract class Message implements MessageInterface
{
    protected $xml;
    protected $toUserName;
    protected $fromUserName;
    protected $message;

    public function __construct($toUserName, $fromUserName, $message)
    {
        $this->toUserName = $toUserName;
        $this->fromUserName = $fromUserName;
        $this->message = $message;
    }

    public function create(): string
    {
        $xml_str = '';
        if (is_array($this->message)) {
            foreach ($this->message as $item) {
                $xml_str .= sprintf($this->xmlStr, $this->toUserName, $this->fromUserName, time(), $item);
            }
        } else {
            $xml_str = sprintf($this->xmlStr, $this->toUserName, $this->fromUserName, time(), $this->message);
        }
        Log::debug($xml_str);
        return $xml_str;
    }

    public function reply(): void
    {
        echo $this->create();
    }
}