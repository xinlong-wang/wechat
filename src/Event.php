<?php


namespace wechat\src;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use wechat\src\data\model\WechatReplyModel;
use wechat\src\data\model\WechatUserinfoModel;
use wechat\src\data\Table;
use wechat\src\reply\Image;
use wechat\src\reply\Text;

class Event
{
    protected $xmlArray = [];

    public function __construct($xmlArray)
    {
        $this->xmlArray = $xmlArray;
    }

    /**
     * 关注
     * @throws \GuzzleHttp\Exception\GuzzleException|\JsonException
     */
    public function subscribe(): void
    {
        $message = WechatReplyModel::where([
            'msg_type' => 'event',
            'event' => 'subscribe'
        ])->value('message')['content'];
        $textMessage = new Text($this->xmlArray['FromUserName'], $this->xmlArray['ToUserName'], $message);

        if (is_array($this->xmlArray)) {
            $table = new Table();
            $table->createWechatUserinfoTable();
            $model = new WechatUserinfoModel();
            $exists = $model->newModelQuery()
                ->where('openid', '=', $this->xmlArray['FromUserName'])
                ->exists();
            $userinfo = OfficialAccounts::getUserInfoByOpenid($this->xmlArray['FromUserName']);
            Log::debug(json_encode($userinfo, JSON_THROW_ON_ERROR));
            if (!$exists) {
                $userinfo['first_subscribe_time'] = time();
                $model::create($userinfo);
            } else {
                $model->newModelQuery()->where('openid', '=', $userinfo['openid'])->update($userinfo);
            }
        }
        $textMessage->reply();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function scan(): void
    {
        Log::debug(json_encode($this->xmlArray, JSON_THROW_ON_ERROR));
        $client = new Client();
        $response = $client->get($_ENV['APP_URL'] . '/api/admin/wechatQrCode/' . $this->xmlArray['EventKey'] . '/' . $this->xmlArray['FromUserName'],
            ['http_errors' => false]);
        $content = $response->getBody()->getContents();
        $result = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        Log::debug($content);
        $textMessage = new Text($this->xmlArray['FromUserName'], $this->xmlArray['ToUserName'],
            $result['data']['push_back_content']);
        $textMessage->reply();
    }

    public function unsubscribe(): void
    {
        $table = new Table();
        $table->createWechatUserinfoTable();
        WechatUserinfoModel::where('openid', '=', $this->xmlArray['FromUserName'])->update(['subscribe' => 0]);
    }

    public function location()
    {
        return '';
    }

    public function click(): void
    {
        Log::debug("点击事件,key:{$this->xmlArray['EventKey']}");
        $image = new Image($this->xmlArray['FromUserName'], $this->xmlArray['ToUserName'], $this->xmlArray['EventKey']);
        $image->reply();
    }
}
