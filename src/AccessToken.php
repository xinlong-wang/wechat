<?php


namespace wechat\src;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;

class AccessToken
{
    private static $url = 'https://api.weixin.qq.com/cgi-bin/token?';

    /**
     * 获取AccessToken
     * @throws \RuntimeException
     * @throws \GuzzleHttp\Exception\GuzzleException|\JsonException
     */
    public static function get(): string
    {
        $appId = $_ENV['WECHAT_APPID'];
        $appSecret = $_ENV['WECHAT_APPSECRET'];
        $redis = new \Redis();
        $redis->connect($_ENV['REDIS_HOST'], $_ENV['REDIS_PORT']);
        $redis->auth($_ENV['REDIS_PASSWORD']);
        $accessToken = $redis->get($appId . '_AccessToken');
        if (!$accessToken) {
            $request = new Request('get', self::$url . http_build_query(['grant_type' => 'client_credential', 'appid' => $appId, 'secret' => $appSecret]));
            $client = new Client();
            $response = $client->send($request, ['http_errors' => false]);
            $content = $response->getBody()->getContents();;
            $result = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
            if (isset($result['errcode'])) {
                Log::debug($content);
                throw new WechatRuntimeException('获取AccessToken失败');
            }
            $accessToken = $result['access_token'];
            $redis->setex($appId . '_AccessToken', 7200, $accessToken);
        }
        return $accessToken;
    }

    /**
     * 重新获取AccessToken
     * @throws \GuzzleHttp\Exception\GuzzleException|\JsonException
     */
    public static function refresh(): string
    {
        $redis = new \Redis();
        $redis->connect($_ENV['REDIS_HOST'], $_ENV['REDIS_PORT']);
        $redis->auth($_ENV['REDIS_PASSWORD']);
        $redis->del($_ENV['WECHAT_APPID'] . '_AccessToken');
        return self::get();
    }
}
