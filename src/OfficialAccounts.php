<?php

namespace wechat\src;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use wechat\src\data\model\WechatLogModel;
use wechat\src\data\model\WechatReplyModel;
use wechat\src\data\model\WechatUserinfoModel;
use wechat\src\data\Table;
use wechat\src\reply\Image;
use wechat\src\reply\Text;

class OfficialAccounts
{
    protected $appId;
    protected $secret;
    protected $token;

    /**
     * OfficialAccounts constructor.
     */
    public function __construct()
    {
        $this->appId = $_ENV['WECHAT_APPID'];
        $this->secret = $_ENV['WECHAT_APPSECRET'];
        $this->token = $_ENV['WECHAT_RESPONSE_TOKEN'];
    }

    /**
     * signature验证通过原样返回
     * @return string
     */
    public function verification(): ?string
    {
        if ($this->checkSignature()) {
            return $_GET['echostr'];
        }
    }

    /**
     * 检验signature
     * @return bool
     */
    private function checkSignature(): bool
    {
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];

        $tmpArr = [$this->token, $timestamp, $nonce];
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);

        return $tmpStr === $signature;
    }

    /**
     * 创建自定义菜单
     * @param string $menu
     * @return bool
     * @throws Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public static function createMenu(string $menu): bool
    {
        $accessToken = AccessToken::get();
        $request = new Request('post',
            "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=$accessToken",
            [], $menu);
        $client = new Client();
        $result = $client->send($request, ['http_errors' => false])->getBody()->getContents();
        $result = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
        if ((int)$result['errcode'] !== 0 && $result['errmsg'] !== 'ok') {
            Log::error('自定义菜单创建失败' . $result['errcode'] . $result['errmsg']);
            return false;
        }
        return true;
    }

    /**
     * @throws \JsonException
     */
    public function responseMessage(): void
    {
        // 防御XML注入攻击
        libxml_disable_entity_loader(true);
        $xml = file_get_contents('php://input');
        Log::debug(json_encode($xml, JSON_THROW_ON_ERROR));
        $xmlArray = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA),
            JSON_THROW_ON_ERROR), true, 512, JSON_THROW_ON_ERROR);
        Log::debug(json_encode($xmlArray, JSON_THROW_ON_ERROR));
        $table = new Table();
        $table->createWechatLogTable();
        if (is_array($xmlArray)) {
            WechatLogModel::create($xmlArray);
        }
        switch ($xmlArray['MsgType']) {
            case 'event':
                $event = strtolower($xmlArray['Event']);
                (new Event($xmlArray))->$event();
                break;
            case 'text':
                $media_ids = WechatReplyModel::where([
                    'key' => $xmlArray['Content']
                ])->value('message');
                echo '';
                if ($media_ids) {
                    foreach ($media_ids as $media_id) {
                        \wechat\src\customer_service\message\Image::send($xmlArray['FromUserName'], $media_id);
                    }
                } else {
                    $message = WechatReplyModel::where(['key' => 'default'])->value('message')['content'];
                    $text = new Text($xmlArray['FromUserName'], $xmlArray['ToUserName'], $message);
                    $text->reply();
                }
                break;
            case 'image':
                break;
        }
    }


    /**
     * 保存二维码
     * @param string $ticket
     * @param string $file_name
     * @param string $save_path
     * @return bool
     * @throws Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public static function saveQrCode(string $ticket, string $file_name = '', string $save_path = './'): bool
    {
        $url = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=%s';
        $url = sprintf($url, urlencode($ticket));
        $client = new Client();
        $response = $client->get($url, ['http_errors' => false]);
        $content = $response->getBody()->getContents();
        $path = '%s%s.jpeg';
        $path = sprintf($path, $save_path, $file_name);
        if (file_put_contents($path, $content)) {
            return true;
        }
        return false;
    }

    /**
     * 发送模板消息
     * @param string $openid
     * @param string $template_id
     * @param string $target_url
     * @param string $top_color
     * @param array  $content
     * @return bool
     * @throws Exception
     */
    public function sendTemplateMessage(
        string $openid,
        string $template_id,
        string $target_url,
        string $top_color,
        array $content
    ): bool {
        $url = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%s';
        $url = sprintf($url, $this->getAccessToken());
        $data = [
            'touser' => $openid,
            'template_id' => $template_id,
            'url' => $target_url,
            'data' => $content
        ];
        $request = new SendRequest($url, json_encode($data));
        $result = json_decode($request->post(), true);
        if ((int)$result['errcode'] !== 0) {
            $this->writeLog('发送模板消息失败', $result['errcode'], $result['errmsg']);
            return false;
        }
        return true;
    }

    /**
     * 通过openID获取用户基本信息
     * @param string $openid
     * @param string $language
     * @return array
     * @throws Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public static function getUserInfoByOpenid(string $openid, string $language = 'zh_CN'): array
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=%s';
        $url = sprintf($url, AccessToken::get(), $openid, $language);
        $request = new Request('get', $url);
        $client = new Client();
        $response = $client->send($request, ['http_errors' => false]);
        $content = $response->getBody()->getContents();
        Log::debug($content);
        return json_decode($content, true);
    }


    public static function userList(string $next_openid = '')
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s&next_openid=%s';
        $url = sprintf($url, AccessToken::get(), $next_openid);
        $request = new Request('get', $url);
        $client = new Client();
        $response = $client->send($request, ['http_errors' => false]);
        $content = $response->getBody()->getContents();
        Log::debug($content);
        return json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }


    public static function saveAllUsers(): void
    {
        $data = self::userList();
        foreach ($data['data']['openid'] as $openid) {
            if (!WechatUserinfoModel::where(['openid' => $openid])->exists()) {
                $userinfo = self::getUserInfoByOpenid($openid);
                WechatUserinfoModel::create($userinfo);
            }
        }
        while (!empty($data['next_openid'])) {
            $data = self::userList($data['next_openid']);
            foreach ($data['data']['openid'] as $openid) {
                if (!WechatUserinfoModel::where(['openid' => $openid])->exists()) {
                    $userinfo = self::getUserInfoByOpenid($openid);
                    WechatUserinfoModel::create($userinfo);
                }
            }
        }
    }
}
