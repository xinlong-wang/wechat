<?php

namespace wechat\src\data\model;

use Illuminate\Database\Eloquent\Model;

class WechatUserinfoModel extends Model
{
    protected $table = 'wechat_userinfo';
    protected $fillable = [
        'openid',
        'nickname',
        'sex',
        'city',
        'country',
        'province',
        'language',
        'headimgurl',
        'first_subscribe_time',
        'subscribe_time',
        'subscribe',
        'unionid',
        'remark',
        'groupid',
        'subscribe_scene',
        'qr_scene',
        'qr_scene_str'
    ];

    protected $casts = [
        'tagid_list' => 'json'
    ];
}
