<?php


namespace wechat\src\data\model;


use Illuminate\Database\Eloquent\Model;

class WechatLogModel extends Model
{
    protected $table = 'wechat_log';
    protected $casts =['EventKey'=>'json'];
    protected $fillable = [
        'ToUserName',
        'FromUserName',
        'CreateTime',
        'MsgType',
        'Content',
        'MsgId',
        'Event',
        'EventKey',
        'Ticket'
    ];
}
