<?php


namespace wechat\src\data\model;


use Illuminate\Database\Eloquent\Model;

class WechatReplyModel extends Model
{
    protected $table = 'wechat_reply';
    protected $casts = [
        'message' => 'json'
    ];
}
