<?php

use Illuminate\Database\Capsule\Manager;

$database = [
    'driver' => $_ENV['DB_CONNECTION'],
    'host' => $_ENV['DB_HOST'],
    'database' => $_ENV['DB_DATABASE'],
    'username' => $_ENV['DB_USERNAME'],
    'password' => $_ENV['DB_PASSWORD'],
    'port' => $_ENV['DB_PORT'],
];
$capsule = new Manager();
$capsule->addConnection($database);
$capsule->setAsGlobal();
$capsule->bootEloquent();