<?php


namespace wechat\src\data;

use Illuminate\Database\Capsule\Manager as Db;

class Table
{
    public function __construct()
    {
        $capsule = new Db();
        $capsule->addConnection(
            [
                'driver' => $_ENV['DB_CONNECTION'],
                'host' => $_ENV['DB_HOST'],
                'database' => $_ENV['DB_DATABASE'],
                'username' => $_ENV['DB_USERNAME'],
                'password' => $_ENV['DB_PASSWORD'],
                'port' => $_ENV['DB_PORT'],
            ]
        );
        $capsule->setAsGlobal();
    }

    /**
     * 创建wechat_log表
     */
    public function createWechatLogTable(): void
    {
        if (!Db::schema()->hasTable('wechat_log')) {
            Db::schema()->create('wechat_log', function ($table) {
                $table->increments('id');
                $table->string('ToUserName', 100)->comment('开发者微信号');
                $table->string('FromUserName', 100)->comment('发送方帐号（一个OpenID）');
                $table->bigInteger('CreateTime')->comment('消息创建时间 （整型）');
                $table->char('MsgType', 50)->default('')->comment('消息类型');
                $table->string('Content')->default('')->comment('消息内容');
                $table->bigInteger('MsgId')->default(0)->comment('消息内容');
                $table->string('Event')->default('')->comment('事件类型，subscribe(订阅)、unsubscribe(取消订阅)');
                $table->json('EventKey')->nullable()->comment('事件KEY值，qrscene_为前缀，后面为二维码的参数值');
                $table->string('Ticket')->default('')->comment('	二维码的ticket，可用来换取二维码图片');
                $table->timestamps();
            });
        }
    }

    public function createWechatUserinfoTable(): void
    {
        if (!Db::schema()->hasTable('wechat_userinfo')) {
            Db::schema()->create('wechat_userinfo', function ($table) {
                $table->increments('id');
                $table->string('openid')->default('')->unique();
                $table->string('nickname')->default('');
                $table->string('sex')->default('')->comment('用户的性别，值为1时是男性，值为2时是女性，值为0时是未知');
                $table->string('city')->default('')->comment('用户所在城市');
                $table->string('country')->default('')->comment('用户所在国家');
                $table->string('province')->default('')->commnet('用户所在省份');
                $table->string('language')->default('')->comment('用户的语言，简体中文为zh_CN');
                $table->string('headimgurl')->default('')->comment('用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。');
                $table->integer('first_subscribe_time')->default(0)->comment('用户首次关注时间');
                $table->integer('subscribe_time')->default(0)->comment('用户关注时间');
                $table->tinyInteger('subscribe')->default(0)->comment('0未关注；1已关注');
                $table->string('unionid')->default('')->comment('只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。');
                $table->string('remark')->default('')->comment('公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注');
                $table->string('groupid')->default('')->comment('用户所在的分组ID（兼容旧的用户分组接口）');
                $table->string('subscribe_scene')->default('')->comment('返回用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION 公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，ADD_SCENE_QR_CODE 扫描二维码，ADD_SCENE_PROFILE_LINK 图文页内名称点击，ADD_SCENE_PROFILE_ITEM 图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_WECHAT_ADVERTISEMENT 微信广告，ADD_SCENE_OTHERS 其他');
                $table->string('qr_scene')->default('')->comment('二维码扫码场景');
                $table->string('qr_scene_str')->default('')->comment('二维码扫码场景描述');
                $table->jsonb('tagid_list')->default('')->comment('用户被打上的标签ID列表');
                $table->timestamps();
            });
        }
    }

    public function createReplyTable(): void
    {
        if (!Db::schema()->hasTable('wechat_reply')) {
            Db::schema()->create('wechat_reply', function ($table) {
                $table->increments('id');
                $table->tinyInteger('type')->default(1)->comment('事件类型，触发时回复对应消息：1：关注；2：消息');
                $table->string('key')->default('')->comment('事件推送的key或text消息的文字');
                $table->string('message')->default('')->comment('要回复的消息');
                $table->timestamps();
            });
        }
    }
}
