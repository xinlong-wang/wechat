<?php

namespace wechat\src\customer_service\message;

interface MessageInterface
{
    /**
     * 发送
     * @param $toUser
     * @param $media_id
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public static function send($toUser, $media_id): void;
}
