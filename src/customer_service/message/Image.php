<?php


namespace wechat\src\customer_service\message;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;
use wechat\src\AccessToken;

class Image implements MessageInterface
{
    protected static $type = 'image';
    protected static $url = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=%s';

    /**
     * 发送
     * @param $toUser
     * @param $media_id
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public static function send($toUser, $media_id): void
    {
        $request = new Request('POST', sprintf(self::$url, AccessToken::get()), [],
            self::buildParams($toUser, $media_id));

        $client = new Client();
        $content = $client->send($request, ['http_errors' => false])->getBody()->getContents();
        Log::debug(PHP_EOL . "发送客服图片消息：$content " . PHP_EOL);
    }

    /**
     * @param $toUser
     * @param $media_id
     * @return false|string
     * @throws \JsonException
     */
    protected static function buildParams($toUser, $media_id): string
    {
        return json_encode(['touser' => $toUser, 'msgtype' => self::$type, 'image' => ['media_id' => $media_id]],
            JSON_THROW_ON_ERROR);
    }
}
