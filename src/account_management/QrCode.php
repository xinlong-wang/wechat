<?php
namespace wechat\src\account_management;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use wechat\src\AccessToken;

/**
 * 生成带参数的二维码
 * Class QrCode
 * @package wechat\src\account_management
 */
class QrCode
{
    private $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s";
    private $method = 'POST';
    private $param;
    private $forever = true;
    private $expire_seconds = 30;


    /**
     * 二维码参数，整型为scene_id  字符串为scene_str
     * @param int|string $param
     * @return void
     */
    public function setParam($param): void
    {
        $this->param = $param;
    }

    /**
     * 设置是否为永久二维码，true:永久 false:临时
     * @param bool $forever
     */
    public function setForever(bool $forever): void
    {
        $this->forever = $forever;
    }

    /**
     * 设置临时二维码过期时间
     * @param int $expire_seconds
     */
    public function setExpireSeconds(int $expire_seconds = 30): void
    {
        $this->expire_seconds = $expire_seconds;
    }

    /**
     * 获取二维码的数据
     * @return false|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException|\JsonException
     */
    public function getData()
    {
        $data = [];
        if (is_int($this->param)) {
            if ($this->forever) {
                $data['action_name'] = "QR_LIMIT_SCENE";
            } else {
                $data['action_name'] = "QR_SCENE";
                $data['expire_seconds'] = $this->expire_seconds;
            }
            $data['action_info'] = ['scene' => ['scene_id' => $this->param]];
        }
        if (is_string($this->param)) {
            if ($this->forever) {
                $data['action_name'] = "QR_LIMIT_STR_SCENE";
            } else {
                $data['action_name'] = "QR_STR_SCENE";
                $data['expire_seconds'] = $this->expire_seconds;
            }
            $data['action_info'] = ['scene' => ['scene_str' => $this->param]];
        }
        $request = new Request($this->method, sprintf($this->url, AccessToken::get()), [], json_encode($data));
        $client = new Client();
        $response = $client->send($request, ['http_errors' => false]);
        $content = $response->getBody()->getContents();
        $result = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        if (isset($result['ticket'], $result['url'])) {
            return $result;
        }
        return false;
    }
}
